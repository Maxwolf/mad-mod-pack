@ECHO OFF

REM Sets the Variable for Conflicts to batch file lives.
REM %0 is the name of the batch file. ~dp gives you the drive and path of the specified argument.
SET WORKING_DIR=%~dp0

REM First search pattern that we will look for.
SET PATTERN0=CONFLICT @

REM Second search pattern that we will look for.
SET PATTERN1=java.lang.IllegalArgumentException

REM Sets the variable file to appropriate file for client or server instance.
SET FMLSERVER=ForgeModLoader-server-0.log

REM Finds all with lines in the log file that match our first pattern.
ECHO Looking for %PATTERN0% > "%WORKING_DIR%\conflicts.txt"
findstr /s /c:"%PATTERN0%" %FMLSERVER% >> "%WORKING_DIR%\conflicts.txt"
if errorlevel 1 ( echo Pattern Not Found "%PATTERN0%")
if not errorlevel 1 ( echo Pattern Recognized "%PATTERN0%")

REM Finds all with lines in the log file that match our second pattern.
ECHO Looking for %PATTERN1% >> "%WORKING_DIR%\conflicts.txt"
findstr /s /c:"%PATTERN1%" %FMLSERVER% >> "%WORKING_DIR%\conflicts.txt"
if errorlevel 1 ( echo Pattern Not Found "%PATTERN1%")
if not errorlevel 1 ( echo Pattern Recognized "%PATTERN1%")

REM Wait for user input before closing.
PAUSE
